package application;
/**
 * Implementarea functiei de calcularea a numarului de vecini pentru un joc cu tabla marginita.
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 28 decembrie 2014
 * @version 1.0
 */
import cell.Cell;

public class BorderNeighbourFinder implements NeighbourFinder {

	@Override
	public int getNeighbours(GameOfLife game, int x, int y) {
		int res = 0;
		for(int i=-1; i <= 1; i++){
			for(int j=-1; j <=1; j++){
				if(i != 0 || j != 0){
					int px = x+i;
					int py = y+j;
					
					if(game.getNetwork().getCellStatus(px, py) == Cell.Status.ALIVE){
						res++;
					}
				}
			}
		}
		return res;
	}

}
