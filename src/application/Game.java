package application;

import gui.*;

/**
 * Clasa abstracta ce defineste functia de ciclare a jocului.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public abstract class Game implements Application, Drawable{
	private boolean isActive = true;
	
	/**
	 * Clasa ce contine constante specifice jocului.
	 *  
     * @author Atanasiu Alexandru-Marian, grupa 324CB
     * @since 23 decembrie 2014
     * @version 1.0
	 *
	 */
	public class Constants{
		public static final double    LOW_TIME = 0.1;
		public static final double MEDIUM_TIME = 0.5;
		public static final double   HIGH_TIME = 1;
	}
	
	/** Functia de rulare continua a executiei programului. */
	@Override
	public final void Loop() throws ApplicationException{
		while(isActive){
			SimulateStep();
			Draw();
		}
		Terminate();
	}
}
