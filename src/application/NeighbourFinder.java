package application;

/**
 * Interfata comuna implementarilor de calculare a numarului de vecini (folosit petru implementarea designului STRATEGY)
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 28 decembrie 2014
 * @version 1.0
 */
public interface NeighbourFinder {
	/** Functie de calcul a numarului de vecini ai unei celule. */
	public int getNeighbours(GameOfLife game, int x, int y);
}
