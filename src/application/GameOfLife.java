package application;


import gui.*;
import cell.*;

/**
 * Clasa care simuleaza executia jocului "Game of life"
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public class GameOfLife extends Game implements Drawable{
	public enum GameType{ BORDER, BORDERLESS }
	
	protected boolean hasGUI = false;
	
	/** Exceptie aruncata in cazul in care datele jocului nu au fost incarcate. */
	@SuppressWarnings("serial")
	private class GameDataNotLoadedException extends ApplicationException {}
	
	/** Exceptie aruncata in cazul in care datele jocului sunt invalide. */
	@SuppressWarnings("serial")
	private class InvalidGameDataException extends ApplicationException {}
	
	/** Clasa ce contine eventualele constante utilizate in cadrul clasei actuale. */
	private class Constants{
		public class ErrorMessages{
			public static final String DATA_NOT_LOADED = "Datele jocului nu au fost incarcate...Va rugam cititi datele jocului si apoi reincercati reinitializare."; 
			public static final String INVALID_DATA = "Date invalide...Va rugam reintroduceti datele simularii.";
		}
	}
	
	/**
	 * Clasa ce implementea InitializationData, pentru a facilita creerea unui container pentru datele interne ale jocului.
     * @author Atanasiu Alexandru-Marian, grupa 324CB
     * @since 23 decembrie 2014
     * @version 1.0
	 */
	public static class GameInitializationData implements Application.InitializationData{
		protected Cell.Status[][] map;
		
		public GameInitializationData(Cell.Status[][] map){ this.map = map; }
		public Cell.Status[][] getMap(){ return map; }
	}
	
	protected gameData data;
	protected CellNetwork network;
	protected GameType type;
	protected GUI frame;
	
	@Override
	public void Initialize(InitializationData gid) throws GameDataNotLoadedException, 
															InvalidGameDataException{
		try {
			network = new CellNetwork(getWidth(), getHeight());
		} catch (Exception e) {
			throw new InvalidGameDataException();
		}
		Checker();		
		try{
			if(!(gid instanceof GameInitializationData)){
				throw new InvalidGameDataException();
			}
			network.Initialization(((GameInitializationData) gid).getMap());
			
			GUI.loadGame(this);
			GUI.Reset();
			frame = GUI.getWindow();
		} catch(Exception e){
			throw new InvalidGameDataException();
		}
	}
	
	@Override
	public void InputData() {
		type = InputFunctions.selectType("Tipul tablei de joc (cu sau fara margini): ", "TIP TABLA");
		int width  = gui.InputFunctions.readInt("Lungimea tablei:" , "LUNGIME");
		int height = gui.InputFunctions.readInt("Latimea tablei:"  , "LATIME" );
		double deltaTime = gui.InputFunctions.selectSpeed("Selectati viteza simularii: ", "VITEZA SIMULARII");
		
		data = new gameData.dataBuilder()
			.withWidth(width)
			.withHeight(height)
			.withDeltaTime(deltaTime)
		.build();
	}

	@Override
	public void SimulateStep() {
		Cell.Status[][] newNetwork = new Cell.Status[getWidth()][getHeight()];
		for(int i=0; i < getWidth(); i++){
			for(int j=0; j < getHeight(); j++){
				int neighbours = getNeighbours(i,j);
				
				if(neighbours < 2 || neighbours > 3){
					newNetwork[i][j] = Cell.Status.DEAD;
				} else if(neighbours == 3){
					newNetwork[i][j] = Cell.Status.ALIVE;
				} else {
					newNetwork[i][j] = network.getCellStatus(i, j);
				}
			}
		}
		try {
			network = new CellNetwork(getWidth(),getHeight());
			network.Initialization(newNetwork);
		} catch (Exception e) {}
		
		Draw();
		
		try {
			Thread.sleep((int)(data.getDeltaTime()*1000));
		} catch (InterruptedException e) {}
		
	}

	/**
	 * Functie ce, pentru in punct de pe tabla jocului, va returna numarul de celule vii invecinate cu acest punct.
	 * @param x coordonata x a punctului cerut
	 * @param y coordonata y a punctului cerut
	 * @return numarul de celule vii invecinate pe care punctul interogat le are.
	 */
	public int getNeighbours(int x, int y){
		NeighbourFinder finder = null;
		if(type == GameType.BORDER){
			finder = new BorderNeighbourFinder();
		}else {
			finder = new BorderlessNeighbourFinder();
		}
		
		return finder.getNeighbours(this, x, y);
	}
	
	@Override
	public void Terminate() {
		data = null;
		network = null;
	}

	
	@Override
	public void Draw() {
		frame.Draw();
	}
	
	//Functii folosite doar pentru testare
	
	/** @param data datele pentru a fi setate jocului*/
	public void setData(gameData data){ this.data = data; }
	
	/** @param type tipul tablei de joc (cu sau fara margine) */
	public void setType(GameType type){ this.type = type; }
	
	/** @return inaltimea tablei */
	public    int getHeight() { return data.getHeight();    }
	/** @return latimea tablei */
	public    int  getWidth() { return data.getWidth();     }
	/** @return timpul dintre iteratiile jocului. */
	public double   getTime() { return data.getDeltaTime(); }
	/** @return reteaua de celule din joc. */
	public CellNetwork getNetwork() { return network; }
	
	//Functie ce verifica integritatea datelor inregistrate
	private void Checker() throws GameDataNotLoadedException,
									InvalidGameDataException{
		if(data == null || network == null){
			System.err.println((new java.util.Date()) + ": " + Constants.ErrorMessages.DATA_NOT_LOADED);
			throw new GameDataNotLoadedException();
		} else if (getWidth() <= 0 || getHeight() <= 0 || getTime() <= 0){
			System.err.println((new java.util.Date())+ ": " + Constants.ErrorMessages.INVALID_DATA);
			throw new InvalidGameDataException();
		}
	}
}