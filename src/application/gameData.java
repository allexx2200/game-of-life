package application;

/**
 * Clasa care contine informatii despre instanta actuala a jocului.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public class gameData{
	private int width, height;
	private double deltaTime;
	
	//Functii setter
	public void setWidth    (int width)        { this.width = width;   }
	public void setHeight   (int height)       { this.height = height; }
	public void setDeltaTime(double deltaTime) { this.deltaTime = deltaTime; }
	
	//Functii getter
	public int getWidth()  { return width; }
	public int getHeight() { return height; }
	public double getDeltaTime() { return deltaTime; }
	
	/**
	 * Clasa ce functioneaza ca FLUENT BUILDER, pentru a usura popularea informatiilor in obiect.
	 * 
	 * @author Atanasiu Alexandru-Marian, grupa 324CB
	 * @since 24 decembrie 2014
	 * @version 1.0
	 */
	public static class dataBuilder{
		private gameData info = new gameData();
		
		//Adauga lungime
		public dataBuilder withWidth(int width){
			info.setWidth(width);
			return this;
		}
		
		//Adauga latime
		public dataBuilder withHeight(int height){
			info.setHeight(height);
			return this;
		}
		
		//Adauga intervalul
		public dataBuilder withDeltaTime(double deltaTime){
			info.setDeltaTime(deltaTime);
			return this;
		}
		
		//Functia de construire
		public gameData build(){
			return info;
		}
	}
}
