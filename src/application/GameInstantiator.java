package application;

import java.io.*;
import java.util.Scanner;

import cell.Cell;

public class GameInstantiator {
	public enum choice{
		RANDOMIZED,
		FILED
	}
	
	
	
	public static void PlayRandomizedGame(){
		GameOfLife game = new GameOfLife();
		
		game.InputData();
		try {
			int width = game.getWidth();
			int height = game.getHeight();
			Cell.Status[][] cellMap = new Cell.Status[width][height];
			
			for(int i=0; i < width; i++){
				for(int j=0; j < height; j++){
					int choice = (int)(Math.random()*2);
					if(choice == 1){
						cellMap[i][j] = Cell.Status.ALIVE;
					} else {
						cellMap[i][j] = Cell.Status.DEAD;
					}
				}
			}
			
			game.Initialize(new GameOfLife.GameInitializationData(cellMap));
			game.Loop();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void PlayFiledGame(String path){
		GameOfLife game;
		try {
			Scanner s = new Scanner(new File(path));
			int width = s.nextInt();
			int height = s.nextInt();
			
			double deltaTime = Game.Constants.LOW_TIME;
			
			game = new GameOfLife();
			gameData data = new gameData.dataBuilder()
				.withWidth(width)
				.withHeight(height)
				.withDeltaTime(deltaTime)
			.build();
			game.setData(data);
			
			int t = s.nextInt();
			GameOfLife.GameType type;
			
			switch(t){
				case(1):
					type = GameOfLife.GameType.BORDERLESS;
					break;
				default:
					type = GameOfLife.GameType.BORDER;
					break;
			}
			
			game.setType(type);
			
			Cell.Status[][] map = test.TestSet.readScanner(s, width, height);
			
			game.Initialize(new GameOfLife.GameInitializationData(map));
			
			game.Loop();
			
		} catch (FileNotFoundException e) {
			System.err.println("Fisierul nu a fost gasit.");
		} catch (Exception e){
			System.err.println("S-a intamplat ceva neasteptat");
		}
	}
	
	public static void PlayFiledGame(){
		PlayFiledGame("conf" + File.separator + "config.txt");
	}
}
