package application;
/**
 * Interfata comuna pentru aplicatii.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public interface Application {
	public interface InitializationData {}
	
	/** Superclasa din care se vor deriva toate exceptiile posibile. */
	@SuppressWarnings("serial")
	public class ApplicationException extends Exception {};
	
	/** Functia de initializare a programului. */
	public void Initialize(InitializationData initData) throws ApplicationException;
	
	/** Functie de citire a datelor sistemului. */
	public void InputData() throws ApplicationException;
	
	/** Functie de avansare cu un pas a starii progrmaului. */
	public void SimulateStep() throws ApplicationException;
	
	/** Functie de executie a aplicatiei. */
	public void Loop() throws ApplicationException;
	
	/** Functie apelata la incheierea executiei programului. */
	public void Terminate() throws ApplicationException;
}
