package cell;
/**
 * Interfata ce defineste functiile unei celule.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public interface Cell {
	/** Enumerare ce defineste starile in care se poate afla celula. */
	public enum Status{
		DEAD,
		ALIVE
	}
	
	/** Functie care ucide celula. */
	public void Kill();
	
	/** Functie care reinvie celula. */
	public void Revive();
	
	/** Functie care returneaza starea in care se afla o anumita celula. */
	public Status getStatus();
}
