package cell;
/**
 * Interfata pentru clasa ce va genera celule.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public interface CellFactory{
	/** Functie de generare a unei celule moarte. */
	public Cell generateDeadCell();
	
	/** Functie de generare a unei celule vii. */
	public Cell generateAliveCell();
}
