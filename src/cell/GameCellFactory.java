package cell;
/**
 * Factory concret de celule.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public class GameCellFactory implements CellFactory {
	/**
	 * Celule concrete construite cu factory-ul dat.
	 * 
	 * @author Atanasiu Alexandru-Marian, grupa 324CB
	 * @since 23 decembrie 2014
	 * @version 1.0
	 */
	private class GameCell implements Cell{
		/* Stadiul in care se afla celula */
		private Cell.Status status;
		
		/**
		 * Constructorul clasei de celule concrete
		 * @param status statutul initial al celulei create.
		 */
		public GameCell(Cell.Status status){ this.status = status; }
		
		@Override
		public void Kill() { this.status = Cell.Status.DEAD; }

		@Override
		public void Revive() { this.status = Cell.Status.ALIVE; }

		@Override
		public Status getStatus() { return status; }
	}
	
	
	@Override
	public Cell generateDeadCell() { return new GameCell(Cell.Status.DEAD); }

	@Override
	public Cell generateAliveCell() { return new GameCell(Cell.Status.ALIVE); }

}
