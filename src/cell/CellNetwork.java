package cell;
/**
 * Clasa ce defineste reteaua de celule a jocului.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public class CellNetwork {
	@SuppressWarnings("serial")
	private class InvalidDimensionsException extends Exception {}
	
	@SuppressWarnings("serial")
	private class InvalidInputedDataException extends Exception {}
	
	/**
	 * Clasa de constante specifice retelei de celule.
	 * @author Atanasiu Alexandru-Marian, grupa 324CB
     * @since 23 decembrie 2014
     * @version 1.0
	 */
	private class Constants{
		public static final String OUT_OF_BOUNDS = ":GameOfLife/CellNetwork - s-a iesit din dimensiunile tablei de joc.";
	}
	
	protected Cell[][] network;
	protected int width, height;
	
	/**
	 * Constructorul retelei de celule
	 * @param width lungimea tablei
	 * @param height inaltimea tablei
	 * @throws InvalidInputedDataException exceptie aruncata in cazul in care dimensiunile tablei nu sunt valide.
	 */
	public CellNetwork(int width, int height) throws InvalidInputedDataException{
		if(width <= 0 || height <= 0){
			throw new InvalidInputedDataException();
		}
		this.width  = width;
		this.height = height;
	}
	
	/**
	 * Functie de initializare
	 * @param statusMap
	 * @throws InvalidDimensionsException exceptie aruncata in cazul in care dimensiuile tablei sunt invalide
	 * @throws InvalidInputedDataException exceptie aruncata in cazul in care datele sunt invalide
	 */
	public void Initialization(Cell.Status[][] statusMap) throws InvalidDimensionsException,
																 InvalidInputedDataException{
		try{
			if(statusMap.length != width){ throw new InvalidDimensionsException(); }
			
			CellFactory factory = new GameCellFactory();
			network = new Cell[width][height];
		
			for(int i=0; i < width; i++){
				if(statusMap[i].length != height){ throw new InvalidDimensionsException(); }
				
				for(int j=0; j < height; j++){
					if(statusMap[i][j] == Cell.Status.ALIVE){
						network[i][j] = factory.generateAliveCell();
					} else {
						network[i][j] = factory.generateDeadCell();
					}
				}
			}
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidDimensionsException();
		}
	}
	
	/**
	 * Functie ce omoara celula de la coordonatele date, daca aceasta nu este deja moarta.
	 * @param i coordonata x a celulei
	 * @param j coordonota y a celulei
	 */
	public void killCell(int i, int j){ 
		try{
			network[i][j].Kill(); 
		} catch(IndexOutOfBoundsException e){
			System.err.println((new java.util.Date()) + Constants.OUT_OF_BOUNDS);
		}
	}
	
	/**
	 * Functie ce invie celula de la coordonatele date, daca aceasta nu este deja vie.
	 * @param i coordonata x a celulei
	 * @param j coordonota y a celulei
	 */
	public void reviveCell(int i, int j){ 
		try{
			network[i][j].Revive(); 
		} catch(IndexOutOfBoundsException e){
			System.err.println((new java.util.Date()) + Constants.OUT_OF_BOUNDS);
		}
	}
	
	/**
	 * @param i coordonata x a celulei
	 * @param j coordonota y a celulei
	 * @return statul celulei de la coordonatele cerute.
	 */
	public Cell.Status getCellStatus(int i, int j){ 
		try{
			return network[i][j].getStatus(); 
		} catch(IndexOutOfBoundsException e){
			//System.err.println((new java.util.Date()) + Constants.OUT_OF_BOUNDS);
			return null;
		}
	}
	
	@Override
	public String toString(){
		String res = "";
		for(int j=0; j < height; j++){
			for(int i=0; i < width; i++){
				if(getCellStatus(i,j) == Cell.Status.ALIVE){
					res = res + "x ";
				} else {
					res = res + "o ";
				}
			}
			res = res + "\n";
		}
		
		
		return res;
	}
}
