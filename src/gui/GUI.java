package gui;

import cell.*;
import application.*;

import java.awt.*;
import javax.swing.*;
/**
 * Clasa implementata SINGLETON, folosita pentru a realiza o interfata grafica programului.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public class GUI extends JFrame implements Drawable{	
	private static final long serialVersionUID = 1L;

	/**
	 * Clasa de constante specifice GUI
	 * @author Atanasiu Alexandru-Marian, grupa 324CB
	 * @since 23 decembrie 2014
	 * @version 1.0
	 */
	private static class constants{
		public static final Color BACKGROUND_COLOR = Color.BLACK;
		public static final Color DEAD_COLOR       = Color.BLACK;
		public static Color ALIVE_COLOR            = Color.BLUE;
		public static final Color LINE_COLOR       = Color.BLACK;
		public static int SQUARE_SIZE              = 5;
		public static final int preferedSize       = 600;
	}
	
	/**
	 * Clasa ce defineste JPanel-ul folosit in frame.
	 * @author Atanasiu Alexandru-Marian, grupa 324CB
	 * @since 23 decembrie 2014
	 * @version 1.0
	 */
	private class canvas extends JPanel{
		private static final long serialVersionUID = 1L;
		protected GUI gui = null;
		protected int width, height;
		protected Cell.Status[][] matrix;
		
		/**
		 * Constructorul de baza al JPanel-ului
		 * @param gui referinta la clasa GUI
		 */
		public canvas(GUI g){
			this.gui = g;
			Initialize();
		}
		
		/**
		 * Functie de initializare
		 */
		private void Initialize(){
			setBackground(constants.BACKGROUND_COLOR);
			width = gui.getGame().getWidth();
			height = gui.getGame().getHeight();
			
			constants.SQUARE_SIZE = Math.min(constants.preferedSize/width, 
										     constants.preferedSize/height);
			if(constants.SQUARE_SIZE < 2){
				constants.SQUARE_SIZE = 2;
			}
			
			
			matrix = new Cell.Status[width][height];
			
			for(int i=0; i < width; i++){
				for(int j=0; j < height; j++){
					matrix[i][j] = Cell.Status.DEAD;
				}
			}
			constants.ALIVE_COLOR = InputFunctions.readColor("Introduceti culoarea celulelor: ", "CULOARE CELULE");			
		}
		
		/**
		 * Functie care face update la harta ce va fi afiasata pe ecran.
		 */
		private void UpdateMap(){
			for(int i=0; i < width; i++){
				for(int j=0; j < height; j++){
					matrix[i][j] = game.getNetwork().getCellStatus(i, j);
				}
			}
		}
		
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(GUI.constants.SQUARE_SIZE*width+1,GUI.constants.SQUARE_SIZE*height+1);
		}
		
		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			UpdateMap();
			
			for(int i=0; i < width; i++){
				for(int j=0; j < height; j++){
					Color c;
					if(matrix[i][j] == Cell.Status.ALIVE){
						c = constants.ALIVE_COLOR;
					} else {
						c = constants.DEAD_COLOR;
					}
					g.setColor(c);
					g.fillRect(i*constants.SQUARE_SIZE,
							   j*constants.SQUARE_SIZE,
							     constants.SQUARE_SIZE,
							     constants.SQUARE_SIZE);
					
					g.setColor(constants.LINE_COLOR);
					g.drawRect(i*constants.SQUARE_SIZE,
							   j*constants.SQUARE_SIZE,
						         constants.SQUARE_SIZE,
						         constants.SQUARE_SIZE);
				}
			}	
		}
	}
	
	
	private static GUI instance = null;
	private static application.GameOfLife game = null;
	
	protected canvas panel;
	
	/**
	 * Functie ce incarca jocul caruia ii va desena executia.
	 * @param g Jocul aferent
	 */
	public static void loadGame(application.GameOfLife g){ 
		game = g;
	}
	
	@SuppressWarnings("serial")
	private class GameNotLoadedException extends Exception {}; 
	
	private GUI(){
		Initialize();
	}
	
	/** Functie de initializare a interfetei grafice. */
	private void Initialize(){
		panel = new canvas(this);
		add(panel);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
	}
	
	/**
	 * Functie ce returneaza referinta la fereastra actuala.
	 * @return referinta actuala.
	 * @throws GameNotLoadedException exceptie aruncata in cazul in care nu s-au incarcat datele anterior.
	 */
	public static GUI getWindow() throws GameNotLoadedException{
		if(game == null){
			throw instance.new GameNotLoadedException();
		}
		
		if(instance == null){
			instance = new GUI();
		}
		return instance;
	}
	
	/** @return reteaua de celule a jocului */
	public CellNetwork getNetwork(){ return game.getNetwork(); }
	
	/** @return referinta la jocul desenat */
	public GameOfLife getGame() { return game; }

	@Override
	public void Draw() { repaint(); }
	
	/** functie ce reseteaza interfata grafica */
	public static void Reset() {
		instance = null;
	}
}
