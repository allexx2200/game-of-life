package gui;

import java.awt.Color;

import application.*;

import javax.swing.JOptionPane;

/**
 * Clasa ce contine functii de citire a datelor (INPUT_BOX, FISIER)
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public class InputFunctions {
	/**
	 * Citeste un numar intreg de la o fereastra de tip pop-up.
	 * @param text Textul de afisat in casuta de input.
	 * @param title Titlul ferestrei.
	 * @return numarul citit
	 */
	public static int readInt(String text, String title){
		int res = 0;
		boolean valid;
		do {
			try{
				res  = Integer.parseInt(JOptionPane.showInputDialog(null, text, title , JOptionPane.PLAIN_MESSAGE));
				valid = true;
				if(res <= 0){
					valid = false;
				}
			} catch (NumberFormatException e){
				valid = false;
			}
		} while(!valid);
		
		return res;
	}
	
	/**
	 * Citeste frecventa jocului de la o fereastra de tip pop-up.
	 * @param text Textul de afisat in casuta de input.
	 * @param title Titlul ferestrei.
	 * @return frecventa jocului
	 */
	public static double selectSpeed(String text, String title){
		String[] choices = {
				"Viteza mare (10Hz)", 
				"Viteza medie( 2Hz)", 
				"Viteza mica ( 1Hz)"};
		String input = (String) JOptionPane.showInputDialog(
				null, 
				title, text, 
				JOptionPane.QUESTION_MESSAGE, null,
		        choices, choices[0]);
		if(input.compareTo(choices[0]) == 0){
			return Game.Constants.LOW_TIME;
		} else if(input.compareTo(choices[1]) == 0){
			return Game.Constants.MEDIUM_TIME;
		} else {
			return Game.Constants.HIGH_TIME;
		}
	}
	
	/**
	 * Citeste tipul jocului de la o fereastra de tip pop-up.
	 * @param text Textul de afisat in casuta de input.
	 * @param title Titlul ferestrei.
	 * @return tipul jocului
	 */
	public static GameOfLife.GameType selectType(String text, String title){
		String[] choices = {
				"TABLA CU MARGINI",
				"TABLA FARA MARGINI"
		};
		String input = (String) JOptionPane.showInputDialog(
				null, 
				title, text, 
				JOptionPane.QUESTION_MESSAGE, null,
		        choices, choices[0]);
		
		if(input.compareTo("TABLA CU MARGINI") == 0){
			return GameOfLife.GameType.BORDER;
		} else {
			return GameOfLife.GameType.BORDERLESS;
		}
		
	}
	
	/**
	 * Citeste o decizie de genul generare test aleator sau dintr-un fisier de configurare
	 * @param text Textul afisat in fereastra de pop-up
	 * @param title Titlul pe care fereastra de pop-up o are
	 * @return decizia introdusa
	 */
	public static application.GameInstantiator.choice selectChoice(String text, String title){
		String[] choices = {
				"CONFIGURATIE SETATA ALEATOR  ",
				"CONFIGURATIE SETATA IN FISIER"
		};
		
		String input = (String) JOptionPane.showInputDialog(
				null, 
				title, text, 
				JOptionPane.QUESTION_MESSAGE, null,
		        choices, choices[0]);
		
		if(input.compareTo("CONFIGURATIE SETATA ALEATOR  ") == 0){
			return application.GameInstantiator.choice.RANDOMIZED;
		} else {
			return application.GameInstantiator.choice.FILED;
		}
	}
	
	/**
	 * Citeste un sir de caractere de la o fereastra e tip pop-up.
	 * @param text Textul afisat in fereastra de pop-up
	 * @param title Titlul pe care fereastra de pop-up o are
	 * @return String-ul citit
	 */
	public static String readString(String text, String title){
		return JOptionPane.showInputDialog(null, text, title , JOptionPane.PLAIN_MESSAGE);
	}
	
	public static Color readColor(String text, String title){
		String[] choices = {
				"ALBASTRU",
				"ROSU",
				"VERDE",
				"GALBEN"
		};
		String input = (String) JOptionPane.showInputDialog(
				null, 
				title, text, 
				JOptionPane.QUESTION_MESSAGE, null,
		        choices, choices[0]);
		
		if(input.compareTo(choices[0]) == 0){
			return Color.BLUE;
		} else if(input.compareTo(choices[1]) == 0){
			return Color.RED;
		} else if(input.compareTo(choices[2]) == 0){
			return Color.GREEN;
		} else {
			return Color.YELLOW;
		}
	}
}
