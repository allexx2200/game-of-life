package gui;
/**
 * Interfata comuna pentru elemente desenabile.
 * 
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 23 decembrie 2014
 * @version 1.0
 */
public interface Drawable {
	/** Functia de desenare a obiectului aferent. */
	public void Draw();
}
