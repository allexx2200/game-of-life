package test;

import cell.*;
import application.*;
import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestSet {
	
	public static final int DEAD = 0;
	public static final int ALIVE = 1;
	
	public static final int BORDER = 0;
	public static final int BORDERLESS = 1;
	
	private int noTests = 10000;
	
	/* Pentru primul test verificam daca datele dorite sunt introduse corespunzator. */
	@Test
	public void testInitialization() {
		for(int k=0; k < noTests; k++){
		
			GameOfLife app = new GameOfLife();
		
			int width = (int)(Math.random() * 100)+1;
			int height = (int)(Math.random() * 100)+1;
			double deltaTime = Game.Constants.LOW_TIME;
		
			gameData data = new gameData.dataBuilder()
				.withWidth(width)
				.withHeight(height)
				.withDeltaTime(deltaTime)
			.build();
		
			app.setData(data);
			
			Cell.Status[][] map = new Cell.Status[width][height];
			
			for(int i=0; i < width; i++){
				for(int j=0; j < height; j++){
					int choice = (int)(Math.random()*2);
					switch(choice){
						case(ALIVE):
							map[i][j] = Cell.Status.ALIVE;
							break;
						default:
							map[i][j] = Cell.Status.DEAD;
					}
				}
			}
			
			
			assertEquals(app.getWidth(), width);
			assertEquals(app.getHeight(), height);
			assertEquals(app.getTime()-deltaTime == 0,true);
			
		}
	}
	
	/* Functie de test a factory-ului de celule */
	@Test
	public void testFactory(){
		cell.CellFactory factory = new cell.GameCellFactory();
		
		Cell c = factory.generateDeadCell();
		assertEquals(c.getStatus(), cell.Cell.Status.DEAD);
		
		c.Revive();
		assertEquals(c.getStatus(), cell.Cell.Status.ALIVE);
		
		c = null;
		
		c = factory.generateAliveCell();
		assertEquals(c.getStatus(), cell.Cell.Status.ALIVE);
		
		c.Kill();
		assertEquals(c.getStatus(), cell.Cell.Status.DEAD);
	
	}
	
	/* Functie de testare a retelei de celule. */
	@Test
	public void testNetwork(){
		for(int k=0; k < noTests; k++){
		
			int width = (int)(Math.random()*100)+1;
			int height = (int)(Math.random()*100)+1;
		
			cell.Cell.Status[][] statusMap = new cell.Cell.Status[width][height];
		
			for(int i=0; i < width; i++){
				for(int j=0; j < height; j++){
					int choice = (int)(Math.random()*2);
					switch(choice){
						case(DEAD):
							statusMap[i][j] = cell.Cell.Status.DEAD;
							break;
						default:
							statusMap[i][j] = cell.Cell.Status.ALIVE;
							break;
					}
				}
			}
		
			try {
				cell.CellNetwork network = new cell.CellNetwork(width, height);
			
				network.Initialization(statusMap);
	
				assertEquals(equivalenceCheck(network,statusMap), true);
				
				int i = (int)(Math.random()*width);
				int j = (int)(Math.random()*height);
			
				network.killCell(i, j);
				assertEquals(network.getCellStatus(i, j), Cell.Status.DEAD);
			
				network.reviveCell(i, j);
				assertEquals(network.getCellStatus(i, j), Cell.Status.ALIVE);
			} catch (Exception e) {
				fail("S-a intamplat ceva neasteptat...");
			}
		}
	}
	
	//Functie de testare a gasirii numarului de vecini cand tabla este marginita
	@Test
	public void testNeighboursBorder(){
		int width = 3;
		int height = 3;
		
		GameOfLife app = new GameOfLife();
		
		double deltaTime = Game.Constants.LOW_TIME;
		
		gameData data = new gameData.dataBuilder()
			.withWidth(width)
			.withHeight(height)
			.withDeltaTime(deltaTime)
		.build();
	
		app.setData(data);
		app.setType(GameOfLife.GameType.BORDER);
		
		Cell.Status[][] map = new Cell.Status[width][height];
		for(int i=0; i < width; i++){
			for(int j=0; j < height; j++){
				map[i][j] = Cell.Status.DEAD;
			}
		}
		
		map[0][0] = Cell.Status.ALIVE;
		map[0][1] = Cell.Status.ALIVE;
		map[1][0] = Cell.Status.ALIVE;
		
		try {
			app.Initialize(new GameOfLife.GameInitializationData(map));
			
			assertEquals(app.getNeighbours(0, 0), 2);
			assertEquals(app.getNeighbours(0, 1), 2);
			assertEquals(app.getNeighbours(1, 0), 2);
			assertEquals(app.getNeighbours(0, 2), 1);
			assertEquals(app.getNeighbours(2, 0), 1);
			assertEquals(app.getNeighbours(1, 1), 3);
			assertEquals(app.getNeighbours(2, 2), 0);
			
		} catch (Exception e){
			fail("S-a intamplat ceva neasteptat");
		}
	}
	
	//Functie de testare a gasirii numarului de vecini cand tabla este nemarginita
	@Test
	public void testNeighbourBorderless(){
		int width = 3;
		int height = 3;
		double deltaTime = Game.Constants.LOW_TIME;
		
		GameOfLife app = new GameOfLife();
		gameData data = new gameData.dataBuilder()
			.withWidth(width)
			.withHeight(height)
			.withDeltaTime(deltaTime)
		.build();

		app.setData(data);
		app.setType(GameOfLife.GameType.BORDERLESS);
		
		Cell.Status[][] map = new Cell.Status[width][height];
		for(int i=0; i < width; i++){
			for(int j=0; j < height; j++){
				map[i][j] = Cell.Status.DEAD;
			}
		}
		
		map[0][0] = Cell.Status.ALIVE;
		map[0][1] = Cell.Status.ALIVE;
		map[1][0] = Cell.Status.ALIVE;
		map[2][0] = Cell.Status.ALIVE;
		
		try {
			app.Initialize(new GameOfLife.GameInitializationData(map));
			
			assertEquals(app.getNeighbours(0, 0), 3);
			assertEquals(app.getNeighbours(0, 1), 3);
			assertEquals(app.getNeighbours(1, 0), 3);
			assertEquals(app.getNeighbours(0, 2), 4);
			assertEquals(app.getNeighbours(2, 0), 3);
			assertEquals(app.getNeighbours(1, 1), 4);
			assertEquals(app.getNeighbours(2, 2), 4);
			
		} catch (Exception e){
			fail("S-a intamplat ceva neasteptat");
		}
	}
	
	//Functie de testare a programului pe un caz de baza foarte simplu.
	@Test
	public void stepTest1(){
		int width = 5;
		int height = 5;
		double deltaTime = Game.Constants.LOW_TIME;
		
		GameOfLife app = new GameOfLife();
		
		gameData data = new gameData.dataBuilder()
			.withWidth(width)
			.withHeight(height)
			.withDeltaTime(deltaTime)
		.build();

		app.setData(data);
		app.setType(GameOfLife.GameType.BORDER);
	
		Cell.Status[][] map = new Cell.Status[width][height];
		for(int i=0; i < width; i++){
			for(int j=0; j < height; j++){
				map[i][j] = Cell.Status.DEAD;
			}
		}
		
		map[1][2] = Cell.Status.ALIVE;
		map[2][2] = Cell.Status.ALIVE;
		map[3][2] = Cell.Status.ALIVE;
		
		
		Cell.Status[][] finalMap = new Cell.Status[width][height];
		for(int i=0; i < width; i++){
			for(int j=0; j < height; j++){
				finalMap[i][j] = Cell.Status.DEAD;
			}
		}
		
		finalMap[2][1] = Cell.Status.ALIVE;
		finalMap[2][2] = Cell.Status.ALIVE;
		finalMap[2][3] = Cell.Status.ALIVE;
		
		try {
			app.Initialize(new GameOfLife.GameInitializationData(map));

			assertEquals(equivalenceCheck(app.getNetwork(),map), true);
			
			app.SimulateStep();
			assertEquals(equivalenceCheck(app.getNetwork(),finalMap), true);
			
			app.SimulateStep();
			assertEquals(equivalenceCheck(app.getNetwork(),map), true);
		
		} catch (Exception e){
			fail("S-a intamplat ceva neasteptat");
		}
	}

	//Functie de testare a tuturor fisierelor de test din directorul /test
	@Test
	public void fileTest(){
		File folder = new File("tests" + File.separator);
		File[] listOfFiles = folder.listFiles();
		int test = 0;
		GameOfLife app = null;
		
		for(int f=listOfFiles.length-1; f >= 0; f--){
			if(listOfFiles[f].isFile() && listOfFiles[f].getName().length() == 10){
				try {
					System.out.print(listOfFiles[f].getName() + "..........");
					
					Scanner s = new Scanner(listOfFiles[f]);
					int width = s.nextInt();
					int height = s.nextInt();
					int noSteps = s.nextInt();
					double deltaTime = Game.Constants.LOW_TIME;
					
					app = new GameOfLife();
					gameData data = new gameData.dataBuilder()
						.withWidth(width)
						.withHeight(height)
						.withDeltaTime(deltaTime)
					.build();
					app.setData(data);
					
					int t = s.nextInt();
					
					GameOfLife.GameType type;
					
					switch(t){
						case(BORDERLESS):
							type = GameOfLife.GameType.BORDERLESS;
							break;
						default:
							type = GameOfLife.GameType.BORDER;
							break;
					}
					
					app.setType(type);
					
					Cell.Status[][] map = readScanner(s, width, height);
					
					app.Initialize(new GameOfLife.GameInitializationData(map));
					
					for(int k=0; k < noSteps; k++){
						map = readScanner(s, width, height);
						app.SimulateStep();			
						assertEquals(equivalenceCheck(app.getNetwork(), map),true);
					}
					
					s.close();
					System.out.println("test a trecut");
				} catch (Exception e) {
					fail("test " + ++test + " failed.");
				}
			}
		}
	}
	
	//Functie de citire a unei matrici de celule
	public static Cell.Status[][] readScanner(Scanner s, int width, int height){
		Cell.Status[][] map = new Cell.Status[width][height];
		
		for(int j=0; j < height; j++){
			for(int i=0; i < width; i++){
				int choice = s.nextInt();
				switch(choice){
					case(ALIVE):
						map[i][j] = Cell.Status.ALIVE;
						break;
					default:
						map[i][j] = Cell.Status.DEAD;
						break;
				}
			}
		}
		
		return map;
	}
	
	//Functie ce testeaza egalitatea intre o retea de celule si o matrice de statuturi de celule
	public boolean equivalenceCheck(CellNetwork network, Cell.Status[][] statusNetwork){
		try{
			for(int i=0; i < statusNetwork.length; i++){
				for(int j=0; j < statusNetwork[i].length; j++){
					if(network.getCellStatus(i, j) != statusNetwork[i][j]){
						return false;
					}
				}
			}
			
			return true;
		} catch (Exception e){
			return false;
		}
	}
}
