package test;
/**
 * Clasa ce contine functia main()
 * @author Atanasiu Alexandru-Marian, grupa 324CB
 * @since 07 ianuarie 2014
 * @version 1.0
 */
public class test {
	public static void main(String args[]){
		application.GameInstantiator.choice c = gui.InputFunctions.selectChoice("Alegeti tipul de test final:", "TIP");
		if(c == application.GameInstantiator.choice.RANDOMIZED){
			application.GameInstantiator.PlayRandomizedGame();
		} else {
			String path = gui.InputFunctions.readString("Introduceti path-ul catre test: ", "PATH");
			application.GameInstantiator.PlayFiledGame(path);
		}
	}
}